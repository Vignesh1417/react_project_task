export { default as LoginPage } from "./login";
export { default as Dashboard } from "./dashBoard";
export { default as Home } from "./home";
export { default as NotFound } from "./notFound";
export { default as TableProperty } from "./properties";
export { default as UpdatedScreen } from "./updatedDetails";

import React from "react";
import { Button, Typography, Stack, Grid, Box, Hidden } from "@mui/material";
import { LocalStorageKeys } from "../../utils";
import { AppRoutes } from "../../router/routes";
import { LoginSuccess } from "../../router/access";
import { LoginStyles } from "./style";
import { TextBox, Password } from "../../components";
import { useNavigate } from "react-router";
import DotImage from "../../assets/dotImage/dotImage";
import Circle_1 from "../../assets/loginCircles/circle_1";
import Circle_2 from "../../assets/loginCircles/circle_2";

export const LoginPage = ({ title = "Sign in", login = "Log In" }) => {
  const navigate = useNavigate();
  const classes = LoginStyles();

  const onLogin = () => {
    localStorage.setItem(LocalStorageKeys.authToken, "authtoken");
    navigate(AppRoutes.dashboardCard);
  };

  React.useEffect(() => {
    if (localStorage.getItem(LocalStorageKeys.authToken)) {
      navigate(LoginSuccess());
    }
  });

  return (
    <>
      <Hidden smDown>
        <div className={classes.root}>
          <Grid container>
            <Grid item xs={12} sm={12} md={12} lg={12}>
              <div className={classes.bannerImageSet}>
                <Grid container className={classes.containerSet}>
                  <Grid
                    className={classes.dummy}
                    item
                    xs={12}
                    sm={12}
                    md={3}
                    lg={3}
                  ></Grid>
                  <Grid item sm={12} md={9} lg={9} className={classes.itemGrid}>
                    <Box className={classes.card}>
                      <Typography className={classes.signIn}>
                        {title}
                      </Typography>
                      <Stack className={classes.inputBox}>
                        <TextBox
                          label="Mobile Number / Email ID"
                          placeholder="Balaganesh@gmail.com"
                        />
                      </Stack>
                      <Stack className={classes.passWord}>
                        <Password />
                      </Stack>
                      <Typography className={classes.forget}>
                        Did you forget your password?{" "}
                        <span className={classes.clickHere}>Click Here</span>
                      </Typography>
                      <Stack
                        direction="row"
                        spacing={1}
                        alignItems="center"
                        justifyContent="center"
                        className={classes.sponsors}
                      >
                        <Typography className={classes.powerBy}>
                          Powered by
                        </Typography>
                        <img
                          src="images/logo@2x.png"
                          alt="logo"
                          className={classes.squareLogo}
                        />
                        <Typography className={classes.propAuto}>
                          Property Automate
                        </Typography>
                      </Stack>
                      <Button
                        variant="contained"
                        fullWidth
                        className={classes.login}
                        onClick={onLogin}
                        type="submit"
                      >
                        {login}
                      </Button>
                    </Box>
                  </Grid>
                </Grid>
              </div>
              <div className={classes.backgroundImg}></div>
              <div className={classes.dot_1}>
                <DotImage />
              </div>
              <div className={classes.dot_2}>
                <DotImage />
              </div>
              <div className={classes.circle_1}>
                <Circle_1 />
              </div>
              <div className={classes.circle_2}>
                <Circle_2 />
              </div>
            </Grid>
          </Grid>
        </div>
      </Hidden>
      <Hidden mdUp>
        <div className={classes.rootMobile}>
          <Grid container>
            <Grid item xs={12} sm={12} md={12} lg={12}>
              <div className={classes.bannerImageSet}>
                <Grid>
                  <Grid className={classes.dummy} item xs={12} sm={12}>
                    <div className={classes.buildImage}>
                      <img
                        src="images/pexels-timur-saglambilek-87223@2x.png"
                        alt="building"
                      />
                    </div>
                  </Grid>
                  <Grid item sm={12} md={9} lg={9} className={classes.itemGridMobile}>
                    <Box className={classes.mobileCard}>
                      <Typography className={classes.signIn}>
                        {title}
                      </Typography>
                      <Stack className={classes.inputBox}>
                        <TextBox
                          label="Mobile Number / Email ID"
                          placeholder="Balaganesh@gmail.com"
                        />
                      </Stack>
                      <Stack className={classes.passWord}>
                        <Password />
                      </Stack>
                      <Typography className={classes.forget}>
                        Did you forget your password?{" "}
                        <span className={classes.clickHere}>Click Here</span>
                      </Typography>
                      <Stack
                        direction="row"
                        spacing={1}
                        alignItems="center"
                        justifyContent="center"
                        className={classes.sponsors}
                      >
                        <Typography className={classes.powerBy}>
                          Powered by
                        </Typography>
                        <img
                          src="images/logo@2x.png"
                          alt="logo"
                          className={classes.squareLogo}
                        />
                        <Typography className={classes.propAuto}>
                          Property Automate
                        </Typography>
                      </Stack>
                      <Button
                        variant="contained"
                        fullWidth
                        className={classes.login}
                        onClick={onLogin}
                        type="submit"
                      >
                        {login}
                      </Button>
                    </Box>
                  </Grid>
                </Grid>
              </div>
            </Grid>
          </Grid>
        </div>
      </Hidden>
    </>
  );
};

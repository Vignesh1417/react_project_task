import React from "react";
import { LoginPage } from "./login";

class LoginPageParent extends React.Component {
  render() {
    return <LoginPage />;
  }
}

export default LoginPageParent;

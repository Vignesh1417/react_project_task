import { makeStyles } from "@mui/styles";

export const LoginStyles = makeStyles((theme) => ({
  card: {
    backgroundColor: "#FFFFFF",
    boxShadow: "0px 8px 24px #0000001F",
    borderRadius: "16px",
    opacity: 1,
    padding: "30px",
    marginRight: "105px",
    width: "416px",
    zIndex: 1,
    [theme.breakpoints.down("md")]: {
      display: "none",
    },
    [theme.breakpoints.up("md")]: {
      width: "314px",
    },
    [theme.breakpoints.up("lg")]: {
      width: "416px",
    },
  },
  signIn: {
    fontSize: "24px",
    textAlign: "left",
    marginBottom: "32px",
    fontWeight: "bold",
  },
  inputBox: {
    marginBottom: "30px",
  },
  passWord: {
    marginBottom: "8px",
  },
  forget: {
    fontSize: "12px",
    textAlign: "right",
    marginBottom: "50px",
    marginTop: "8px",
  },
  clickHere: {
    color: "#5078E1",
    cursor: "pointer",
  },
  powerBy: {
    color: "#98A0AC",
    fontSize: "10px",
  },
  propAuto: {
    color: "#4E5A6B",
    fontSize: "12px",
  },
  sponsors: {
    marginBottom: "24px",
  },
  login: {
    borderRadius: "12px",
    backgroundColor: "#5078E1",
    textTransform: "initial",
    "&:hover": {
      backgroundColor: "#5078E1",
    },
  },
  squareLogo: {
    width: "24px",
    height: "17px",
  },
  backgroundImg: {
    backgroundImage: `url("images/pexels-timur-saglambilek-87223@2x.png")`,
    height: "100vh",
    backgroundSize: "1115px 699px",
    marginLeft: "-168px",
    backgroundRepeat: "no-repeat",
    marginTop: "-570px",
    position: "relative",
    [theme.breakpoints.down("md")]: {
      display: "none",
    },
    [theme.breakpoints.up("md")]: {
      marginTop: "-500px",
      marginLeft: "-48px",
      backgroundSize: "780px 542px",
    },
    [theme.breakpoints.up("lg")]: {
      backgroundSize: "1115px 699px",
      marginLeft: "-168px",
      marginTop: "-570px",
    },
  },

  bannerImageSet: {
    display: "flex",
    height: "100vh",
  },

  dot_1: {
    "& svg": {
      position: "absolute",
      top: "9px",
      left: "700px",
      [theme.breakpoints.down("md")]: {
        display: "none",
      },
      [theme.breakpoints.up("md")]: {
        left: "539px",
      },
      [theme.breakpoints.up("lg")]: {
        left: "700px",
      },
    },
  },

  dot_2: {
    "& svg": {
      position: "absolute",
      bottom: "14px",
      right: "47px",
      [theme.breakpoints.down("md")]: {
        display: "none",
      },
      [theme.breakpoints.up("md")]: {
        right: "48px",
      },
      [theme.breakpoints.up("lg")]: {
        right: "47px",
      },
    },
  },

  circle_1: {
    "& svg": {
      position: "absolute",
      bottom: "0px",
      left: "-137px",
      [theme.breakpoints.down("md")]: {
        display: "none",
      },
      [theme.breakpoints.down("sm")]: {
        display: "none",
      },
      [theme.breakpoints.down("md")]: {
        display: "none",
      },
    },
  },

  root: {
    [theme.breakpoints.down("md")]: {
      display: "none",
    },
  },

  circle_2: {
    "& svg": {
      position: "absolute",
      top: "-105px",
      left: "96px",
      [theme.breakpoints.down("md")]: {
        display: "none",
      },
      [theme.breakpoints.up("md")]: {
        top: "-115px",
        left: "50px",
      },
      [theme.breakpoints.up("lg")]: {
        top: "-105px",
        left: "96px",
      },
    },
  },

  containerSet: {
    // [theme.breakpoints.down("md")]: {
    //   display: "block",
    // },
  },

  dummy: {
    // [theme.breakpoints.down("sm")]: {
    //   display: "none",
    // },
    // [theme.breakpoints.down("md")]: {
    //   display: "none",
    // },
  },

  itemGrid: {
    backgroundColor: "#5078E1",
    display: "flex",
    justifyContent: "end",
    alignItems: "center",
    // [theme.breakpoints.down("md")]: {
    //   height: "100vh",
    //   justifyContent: "center",
    // },

    [theme.breakpoints.down("md")]: {
      display: "none",
    },
  },

  buildImage: {
    "& img": {
      width: "100%",
      backgroundColor: "#5078E1",
    },
  },

  mobileCard: {
    backgroundColor: "#FFFFFF",
    boxShadow: "0px 8px 24px #0000001F",
    borderRadius: "16px",
    opacity: 1,
    padding: "30px",
    width: "100%",
    zIndex: 1,
  },
}));

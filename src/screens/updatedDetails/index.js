import React from "react";
import { WithNavBars } from "../../HOCs";
import { UpdatedDetails } from "./updatedDetails";

class UpdatedScreen extends React.Component {
  render() {
    return <UpdatedDetails />;
  }
}

export default WithNavBars(UpdatedScreen);

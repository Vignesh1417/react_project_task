import React from "react";
import { WithNavBars } from "../../HOCs";
import { Dashboard } from "./dashBoard";

class DashboardParent extends React.Component {
  render() {
    return <Dashboard />;
  }
}

export default WithNavBars(DashboardParent);

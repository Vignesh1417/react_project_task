import React from "react";
import {
  Typography,
  Box,
  Stack,
  Grid,
  Divider,
  Tabs,
  Tab,
} from "@mui/material";
import { DashBoardStyles } from "./style";
import { LocalStorageKeys } from "../../utils";
import { useNavigate } from "react-router-dom";
import PropTypes from "prop-types";
import Charts from "../../components/graphCard";
import { AppRoutes } from "../../router/routes";
import { Search } from "../../components/search";
import { AddIconImg, Outline, ShowingIcon } from "../../assets";
import { CustomizeTable } from "../../components/table";

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}

export const Dashboard = (props) => {
  const navigate = useNavigate();
  const classes = DashBoardStyles();

  const onLogin = () => {
    localStorage.setItem(LocalStorageKeys.authToken, "authtoken");
    navigate(AppRoutes.home);
  };

  var cardMapping = [
    {
      number: "14",
      label: "Active Properties",
      img: "images/greenHouse.svg",
    },
    {
      number: "06",
      label: "Blocks",
      img: "images/Leads.png",
    },
    {
      number: "12",
      label: "Floors",
      img: "images/parking.svg",
    },
    {
      number: "14",
      label: "Residents",
      img: "images/sandHome.svg",
    },
    {
      number: "10",
      label: "Active Unit",
      img: "images/blueHouse.png",
    },
    {
      number: "03",
      label: "Vacant",
      img: "images/tickHouse.png",
    },
    {
      number: "17",
      label: "Reserved",
      img: "images/LeadSeoncd.png",
    },
    {
      number: "45",
      label: "Occupied",
      img: "images/active.png",
    },
  ];
  var generalRequest = [
    {
      title: "Water Leakage Repair",
      subTitle: "Maintenance",
      date: "22 Jan 21",
      codeId: "K-F01-U207",
    },
    {
      title: "Electricity Volatage Drop",
      subTitle: "Maintenance",
      date: "18 Jan 21",
      codeId: "K-F01-U977",
    },
    {
      title: "Gas Leakage Repair",
      subTitle: "Maintenance",
      date: "28 Jan 22",
      codeId: "K-F02-U222",
    },
    {
      title: "Water Leakage Repair",
      subTitle: "Maintenance",
      date: "11 Jan 21",
      codeId: "K-L01-U877",
    },
    {
      title: "Electricity Volatage Drop",
      subTitle: "Maintenance",
      date: "01 Jan 22",
      codeId: "K-K01-U277",
    },
  ];
  var maintenance = [
    {
      title: "Gas Leakage Repair",
      subTitle: "Maintenance",
      date: "22 Jan 21",
      codeId: "K-F01-U277",
    },
    {
      title: "Water Leakage Repair",
      subTitle: "Maintenance",
      date: "22 Jan 21",
      codeId: "K-F01-U277",
    },
    {
      title: "Electricity Volatage Drop",
      subTitle: "Maintenance",
      date: "22 Jan 21",
      codeId: "K-F01-U277",
    },
    {
      title: "Water Leakage Repair",
      subTitle: "Maintenance",
      date: "22 Jan 21",
      codeId: "K-F01-U277",
    },
    {
      title: "Electricity Volatage Drop",
      subTitle: "Maintenance",
      date: "22 Jan 21",
      codeId: "K-F01-U277",
    },
  ];
  const dataCard = [
    {
      title: "Property Types",
      expandIcon: "/images/enlarge.png",
      type: "pie",
      series: [44, 55, 13, 33],
      options: {
        chart: {
          width: "100%",
          height: 350,
          type: "pie",
        },
        dataLabels: {
          enabled: false,
        },
        labels: ["Vacant", "Occupied", "Reserved", "Listed"],
        responsive: [
          {
            breakpoint: 2000,
            options: {
              chart: {
                height: 350,
                type: "pie",
              },
              legend: {
                position: "bottom",
              },
            },
          },
          {
            breakpoint: 1000,
            options: {
              chart: {
                height: 350,
                type: "pie",
              },
              legend: {
                position: "bottom",
              },
            },
          },
          {
            breakpoint: 600,
            options: {
              chart: {
                type: "pie",
              },
              legend: {
                position: "bottom",
              },
            },
          },
          {
            breakpoint: 480,
            options: {
              chart: {},
              legend: {
                position: "bottom",
              },
            },
            legend: {
              position: "bottom",
            },
          },
        ],
      },
    },
    {
      title: "Unit Category",
      expandIcon: "/images/enlarge.png",
      type: "bar",
      height: 300,
      width: "100%",
      series: [
        {
          name: "PRODUCT A",
          data: [44, 55, 41, 67, 22],
        },
        {
          name: "PRODUCT B",
          data: [13, 23, 20, 8, 13],
        },
        {
          name: "PRODUCT C",
          data: [11, 17, 15, 15, 21],
        },
        {
          name: "PRODUCT D",
          data: [21, 7, 25, 13, 22],
        },
      ],
      options: {
        chart: {
          type: "bar",
          height: 350,
          stacked: true,
          toolbar: {
            show: false,
          },
          zoom: {
            enabled: false,
          },
        },
        responsive: [
          {
            breakpoint: 480,
            options: {},
          },
        ],
        plotOptions: {
          bar: {
            horizontal: false,
          },
        },
        xaxis: {
          type: "datetime",
          categories: [
            "01/01/2011 GMT",
            "01/02/2011 GMT",
            "01/03/2011 GMT",
            "01/04/2011 GMT",
            "01/05/2011 GMT",
          ],
        },
        legend: {
          position: "bottom",
          offsetY: 40,
        },
        fill: {
          opacity: 1,
        },
      },
    },
    {
      title: "Vacant Units By Property",
      expandIcon: "/images/enlarge.png",
      type: "bar",
      height: 300,
      series: [
        {
          data: [400, 430, 448, 470, 540],
        },
      ],
      options: {
        chart: {
          type: "bar",
          height: 350,
        },
        plotOptions: {
          bar: {
            borderRadius: 4,
            horizontal: true,
          },
        },
        dataLabels: {
          enabled: true,
        },
        legend: {
          show: true,
        },
        xaxis: {
          categories: [
            "South Korea",
            "Canada",
            "United Kingdom",
            "Netherlands",
            "Italy",
          ],
        },
      },
    },
    {
      title: "Property Types",
      expandIcon: "/images/enlarge.png",
      type: "donut",
      height: 350,
      series: [44, 55],
      options: {
        chart: {
          width: "100%",
          height: "100%",
          type: "donut",
        },
        dataLabels: {
          enabled: false,
        },
        labels: ["Residential", "Commercial"],
        responsive: [
          {
            breakpoint: 2000,
            options: {
              chart: {
                type: "donut",
              },
              legend: {
                position: "bottom",
              },
            },
          },
          {
            breakpoint: 1000,
            options: {
              chart: {
                type: "donut",
              },
              legend: {
                position: "bottom",
              },
            },
          },
          {
            breakpoint: 600,
            options: {
              chart: {
                type: "donut",
              },
              legend: {
                position: "bottom",
              },
            },
          },
          {
            breakpoint: 480,
            options: {
              chart: {},
              legend: {
                position: "bottom",
              },
            },
          },
        ],
        legend: {
          position: "bottom",
          offsetY: 5,
        },
      },
    },
  ];

  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <Box className={classes.root}>
      <Box className={classes.createPortion}>
        <Grid container spacing={2}>
          {cardMapping?.map((val) => {
            return (
              <Grid item xs={6} sm={4} md={1.5} lg={1.5}>
                <Box className={classes.propertyDetails}>
                  <Stack
                    direction={"row"}
                    alignItems={"center"}
                    justifyContent={"space-between"}
                    mb={2}
                  >
                    <Typography className={classes.cardNumber}>
                      {val.number}
                    </Typography>
                    <img src={val.img} alt="img" className={classes.cardImg} />
                  </Stack>
                  <Typography className={classes.cardLabel}>
                    {val.label}
                  </Typography>
                </Box>
              </Grid>
            );
          })}
        </Grid>
      </Box>
      <Box className={classes.createGraphCard}>
        <Grid container spacing={2}>
          {dataCard?.map?.((item) => {
            return (
              <Grid item lg={3} md={6} sm={6} xs={12}>
                <Box className={classes.bigCards}>
                  <Box className={classes.cardHead}>
                    <Typography className={classes.headTitle}>
                      {item?.title}
                    </Typography>
                    <img src={item?.expandIcon} alt="expandIcon" />
                  </Box>
                  <Charts data={item} />
                </Box>
              </Grid>
            );
          })}
        </Grid>
      </Box>
      <Box className={classes.createGraphCard}>
        <Grid container spacing={2}>
          <Grid item xs={12} sm={12} md={6} lg={6}>
            <Box className={classes.requestDetails}>
              <Box className={classes.requetsBox} p={2} pb={0}>
                <Stack
                  direction={"row"}
                  alignItems={"center"}
                  className={classes.parentRequest}
                  divider={
                    <Divider
                      orientation="vertical"
                      flexItem
                      className={classes.borderRightStyling}
                    />
                  }
                  spacing={2}
                >
                  <Box mr={2}>
                    <Typography className={classes.generalReq} pb={0.5}>
                      General Requests
                    </Typography>
                    <Typography className={classes.generalNumber}>
                      12
                    </Typography>
                  </Box>
                  <Box>
                    <Typography className={classes.generalReq} pb={0.5}>
                      Maintaince
                    </Typography>
                    <Typography className={classes.generalNumber}>
                      15
                    </Typography>
                  </Box>
                </Stack>
              </Box>
              <Box sx={{ width: "100%" }}>
                <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
                  <Tabs
                    value={value}
                    onChange={handleChange}
                    aria-label="basic tabs example"
                    className={classes.tabParent}
                  >
                    <Tab
                      className={classes.switchingTab}
                      label="General Requests"
                      {...a11yProps(0)}
                    />
                    <Tab
                      className={classes.switchingTab}
                      label="Maintenance"
                      {...a11yProps(1)}
                    />
                  </Tabs>
                </Box>
                {/* general request tab */}
                <TabPanel value={value} index={0}>
                  <Box className={classes.searchParent}>
                    <Stack
                      direction={"row"}
                      alignItems={"center"}
                      justifyContent={"space-between"}
                      pb={1.5}
                    >
                      <Typography className={classes.requestNumbers}>
                        General Requests (12)
                      </Typography>
                      <Typography className={classes.viewAll}>
                        View All
                      </Typography>
                    </Stack>
                    <Stack mb={1.5}>
                      <Search />
                    </Stack>
                    <Stack className={classes.overflowTypes}>
                      {generalRequest?.map((val) => {
                        return (
                          <>
                            <Stack
                              direction={"row"}
                              alignItems={"center"}
                              justifyContent={"space-between"}
                              pb={1.5}
                            >
                              <Box>
                                <Typography
                                  className={classes.waterLeak}
                                  mb={0.6}
                                >
                                  {val?.title}
                                </Typography>
                                <Typography
                                  className={classes.dateAndType}
                                  display={"flex"}
                                  alignItems={"center"}
                                >
                                  {val?.subTitle}
                                  <span className={classes.dot}></span>
                                  {val?.date}
                                  <span className={classes.dot}></span>
                                  {val?.codeId}
                                </Typography>
                              </Box>
                              <Box display={"flex"} alignItems={"center"}>
                                <Stack>
                                  <Outline />
                                </Stack>
                                <Stack ml={2}>
                                  <ShowingIcon />
                                </Stack>
                              </Box>
                            </Stack>
                          </>
                        );
                      })}
                    </Stack>
                  </Box>
                </TabPanel>
                <TabPanel value={value} index={1}>
                  <Box className={classes.searchParent}>
                    <Stack
                      direction={"row"}
                      alignItems={"center"}
                      justifyContent={"space-between"}
                      pb={1.5}
                    >
                      <Typography className={classes.requestNumbers}>
                        Maintenance (15)
                      </Typography>
                      <Typography className={classes.viewAll}>
                        View All
                      </Typography>
                    </Stack>
                    <Stack mb={1.5}>
                      <Search />
                    </Stack>
                    <Stack className={classes.overflowTypes}>
                      {maintenance?.map((val) => {
                        return (
                          <>
                            <Stack
                              direction={"row"}
                              alignItems={"center"}
                              justifyContent={"space-between"}
                              pb={1.5}
                            >
                              <Box>
                                <Typography
                                  className={classes.waterLeak}
                                  mb={0.6}
                                >
                                  {val?.title}
                                </Typography>
                                <Typography
                                  className={classes.dateAndType}
                                  display={"flex"}
                                  alignItems={"center"}
                                >
                                  {val?.subTitle}
                                  <span className={classes.dot}></span>
                                  {val?.date}
                                  <span className={classes.dot}></span>
                                  {val?.codeId}
                                </Typography>
                              </Box>
                              <Box display={"flex"} alignItems={"center"}>
                                <Stack>
                                  <Outline />
                                </Stack>
                                <Stack ml={2}>
                                  <ShowingIcon />
                                </Stack>
                              </Box>
                            </Stack>
                          </>
                        );
                      })}
                    </Stack>
                  </Box>
                </TabPanel>
              </Box>
            </Box>
          </Grid>
          <Grid item xs={12} sm={12} md={6} lg={6}>
            <Box className={classes.propertyDetails}>
              <Stack>
                <Typography className={classes.tableLabel} pb={2}>
                  Occupancy By Property
                </Typography>
                <Stack className={classes.tableWithAddIcon}>
                  <CustomizeTable />
                  <Box className={classes.addIconButton}>
                    <AddIconImg
                      sx={{ width: "64px", height: "64px" }}
                      onClick={onLogin}
                    />
                  </Box>
                </Stack>
              </Stack>
            </Box>
          </Grid>
        </Grid>
      </Box>
    </Box>
  );
};

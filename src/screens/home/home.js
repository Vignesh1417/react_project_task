import React from "react";
import {
  Button,
  Typography,
  Box,
  Stack,
  Grid,
  IconButton,
  Checkbox,
} from "@mui/material";
import RadioButtonUncheckedIcon from "@mui/icons-material/RadioButtonUnchecked";
import CheckCircleIcon from "@mui/icons-material/CheckCircle";
import { useStyles } from "./style";
import { LocalStorageKeys } from "../../utils";
import { useNavigate } from "react-router-dom";
import { AppRoutes } from "../../router/routes";
import { TextBox, Dropdown, DateShower } from "../../components";
import ReactQuill from "react-quill";
import "react-quill/dist/quill.snow.css";
import { DownArrow, HomeLogo } from "../../assets";

export const Home = (props) => {
  const navigate = useNavigate();
  const classes = useStyles();

  const onLogOut = () => {
    localStorage.removeItem(LocalStorageKeys.authToken);
    navigate(AppRoutes.dashboardCard);
  };
  const onCreate = () => {
    localStorage.removeItem(LocalStorageKeys.authToken);
    navigate(AppRoutes.propTable);
  };
  const label = { inputProps: { "aria-label": "Checkbox demo" } };
  return (
    <Box className={classes.root}>
      <Stack
        direction="row"
        spacing={1}
        alignItems="center"
        className={classes.headerCreate}
      >
        <Box className={classes.arrowColor}>
          <DownArrow onClick={onLogOut} />
        </Box>
        <Typography className={classes.headerText}>
          Create New Property
        </Typography>
      </Stack>
      <Box className={classes.createPortion}>
        <Grid container>
          <Grid
            item
            xs={12}
            mb-xs={1}
            sm={2.5}
            md={2.3}
            className={classes.propertyImg}
            justifyContent="center"
          >
            <Stack alignItems={"center"} spacing={2} p={2}>
              <Typography className={classes.uploadingImage}>
                PROPERTY IMAGE
              </Typography>
              <Box className={classes.iconBuilding}>
                <IconButton sx={{ width: "100%" }}>
                  <HomeLogo />
                </IconButton>
              </Box>
              <Box
                sx={{
                  width: "100%",
                  display: "flex",
                  justifyContent: "center",
                }}
              >
                <Button variant="outlined" className={classes.outlinedButton}>
                  {" "}
                  Upload Image{" "}
                </Button>
              </Box>
            </Stack>
          </Grid>
          <Grid xs={12} sm={9} md={9.5} className={classes.propertyDetails}>
            <Stack p={2}>
              <Typography className={classes.propertyHeader} mb={1}>
                PROPERTY DETAILS
              </Typography>
              <Grid container spacing={2} mb={1}>
                <Grid item xs={12} sm={4} md={3}>
                  <Dropdown label="Company Name" placeholder="Company Name" />
                </Grid>
                <Grid item xs={12} sm={4} md={3}>
                  <TextBox
                    label="Property Name"
                    defaultValue="Rubix Appartment"
                  />
                </Grid>
                <Grid item xs={12} sm={4} md={3}>
                  <Dropdown label="Payment Period" placeholder="Daily" />
                </Grid>
                <Grid item xs={12} sm={4} md={3}>
                  <Dropdown label="Status" placeholder="Active" />
                </Grid>
              </Grid>
              <Stack>
                <label className={classes.label}>Property Description</label>
                <ReactQuill
                  theme="snow"
                  className={classes.Quill}
                  placeholder=""
                />
              </Stack>
            </Stack>
          </Grid>
        </Grid>
      </Box>
      <Box className={classes.addressType}>
        <Grid container>
          <Grid item xs={12} className={classes.propertyImg}>
            <Stack p={2}>
              <Grid container spacing={2} mb={1}>
                <Grid item xs={12} sm={4} md={2}>
                  <TextBox
                    disable="true"
                    label="Property Type"
                    defaultValue="Apartment"
                  />
                </Grid>
                <Grid item xs={12} sm={4} md={2}>
                  <Dropdown
                    label="Property Purpose"
                    placeholder="Residential"
                  />
                </Grid>
                <Grid item xs={12} sm={4} md={2}>
                  <Dropdown label="Revenue Type" placeholder="Revenue Type" />
                </Grid>
                <Grid item xs={12} sm={4} md={2}>
                  <Dropdown label="Measurement Unit" placeholder="Sq. Ft" />
                </Grid>
                <Grid item xs={12} sm={4} md={2}>
                  <TextBox
                    label="Carpet Area"
                    defaultValue="10000"
                    adornment="Sq. Ft"
                  />
                </Grid>
                <Grid item xs={12} sm={4} md={2}>
                  <TextBox
                    label="Total Area"
                    defaultValue="165480"
                    adornment="Sq. Ft"
                  />
                </Grid>
                <Grid item xs={12} sm={4} md={2}>
                  <DateShower label="Year Built" />
                </Grid>
                <Grid item xs={12} sm={4} md={2}>
                  <DateShower label="Handover Date" />
                </Grid>
                <Grid item xs={12} sm={4} md={2.2}>
                  <Stack>
                    <label className={classes.label}>Public Listing</label>
                    <Stack direction="row" spacing={0.7}>
                      <Button
                        variant="outlined"
                        className={classes.outlinedButton}
                      >
                        {" "}
                        None{" "}
                      </Button>
                      <Button
                        variant="outlined"
                        className={classes.outlinedButton}
                      >
                        {" "}
                        Private{" "}
                      </Button>
                      <Button
                        variant="contained"
                        className={classes.containedButton}
                      >
                        {" "}
                        Public{" "}
                      </Button>
                    </Stack>
                  </Stack>
                </Grid>
                <Grid item xs={12} sm={4} md={2}>
                  <Stack>
                    <label className={classes.label}>Pets Allowed</label>
                    <Box>
                      <Checkbox
                        className={classes.checkboxStyle}
                        icon={<RadioButtonUncheckedIcon />}
                        checkedIcon={<CheckCircleIcon />}
                        {...label}
                        defaultChecked
                      />
                    </Box>
                  </Stack>
                </Grid>
              </Grid>
            </Stack>
          </Grid>
        </Grid>
      </Box>
      {/* address */}
      <Box className={classes.addressType}>
        <Grid container>
          <Grid item xs={12} className={classes.propertyImg}>
            <Stack p={2}>
              <Typography className={classes.propertyHeader} mb={1}>
                Address
              </Typography>
              <Grid container spacing={2}>
                <Grid item xs={12} sm={12} md={5}>
                  <Stack>
                    <img
                      src="/images/mapDemo.png"
                      alt="map"
                      className={classes.map}
                    />
                  </Stack>
                </Grid>
                <Grid item xs={12} sm={12} md={7}>
                  <Grid container spacing={2}>
                    <Grid item xs={12} sm={4} md={2}>
                      <TextBox label="Door Number" defaultValue="192" />
                    </Grid>
                    <Grid item xs={12} sm={4} md={6}>
                      <TextBox
                        label="Address Line 1"
                        defaultValue="East Coast Rd"
                      />
                    </Grid>
                    <Grid item xs={12} sm={4} md={4}>
                      <TextBox
                        defaultValue="Main Road"
                        label="Address Line 2"
                      />
                    </Grid>
                    <Grid item xs={12} sm={4} md={3}>
                      <TextBox label="Landmark" defaultValue="Yaa Briyani" />
                    </Grid>
                    <Grid item xs={12} sm={4} md={3}>
                      <Dropdown label="Area" placeholder="Adyar" />
                    </Grid>
                    <Grid item xs={12} sm={4} md={3}>
                      <Dropdown label="City" placeholder="Chennai" />
                    </Grid>
                    <Grid item xs={12} sm={4} md={3}>
                      <Dropdown label="State" placeholder="Tamil Nadu" />
                    </Grid>
                    <Grid item xs={12} sm={4} md={3}>
                      <Dropdown label="Country" placeholder="India" />
                    </Grid>
                    <Grid item xs={12} sm={4} md={3}>
                      <TextBox label="Pincode" defaultValue="1256543" />
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </Stack>
          </Grid>
        </Grid>
      </Box>
      <Box className={classes.mapType}>
        <Grid container>
          <Grid item xs={12} className={classes.propertyImg}>
            <Stack p={2}>
              <Typography className={classes.propertyHeader} mb={1}>
                CONTACT & OTHER INFORMATION
              </Typography>
              <Grid container spacing={2}>
                <Grid item xs={12} sm={4} md={2}>
                  <TextBox label="Door Number" defaultValue="192" />
                </Grid>
                <Grid item xs={12} sm={4} md={2}>
                  <TextBox
                    label="Address Line 1"
                    defaultValue="East Coast Rd"
                  />
                </Grid>
                <Grid item xs={12} sm={4} md={4}>
                  <TextBox
                    label="Website"
                    defaultValue="propertyautomate.com"
                  />
                </Grid>
                <Grid item xs={12} sm={4} md={4}>
                  <TextBox
                    label="Email Address"
                    defaultValue="mail@propertyautomate.com"
                  />
                </Grid>
              </Grid>
            </Stack>
          </Grid>
        </Grid>
      </Box>
      <Stack
        direction="row"
        spacing={1}
        alignItems="center"
        className={classes.footerButton}
      >
        <Stack spacing={1} direction="row">
          <Button variant="outlined" className={classes.outlinedButton}>
            {" "}
            Cancel
          </Button>
          <Button
            variant="contained"
            className={classes.containedButton}
            onClick={onCreate}
          >
            {" "}
            Create{" "}
          </Button>
        </Stack>
      </Stack>
    </Box>
  );
};

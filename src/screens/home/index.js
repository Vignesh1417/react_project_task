import React from "react";
import { WithNavBars } from "../../HOCs";
import { Home } from "./home";

class HomeParent extends React.Component {
  render() {
    return <Home />;
  }
}

export default WithNavBars(HomeParent);

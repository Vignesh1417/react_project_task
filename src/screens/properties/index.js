import React from "react";
import { WithNavBars } from "../../HOCs";
import { TableProperty } from "./propertyTable";

class PropTable extends React.Component {
  render() {
    return <TableProperty />;
  }
}

export default WithNavBars(PropTable);

import { makeStyles } from "@mui/styles";

export const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: "#F5F7FA",
    marginTop: "5%",
    overflow: "hidden",
    height: "100vh",
    overflowY: "scroll",
  },
  createPortion: {
    backgroundColor: "#F2F4F7",
    padding: "18px 24px 16px 24px",
    [theme.breakpoints.down("md")]: {
      marginTop: "7%",
    },
    [theme.breakpoints.down("sm")]: {
      marginTop: "14%",
    },
  },
  createGraphCard: {
    backgroundColor: "#F2F4F7",
    padding: "4px 24px 16px 24px",
  },
  propertyDetails: {
    boxShadow: "0px 3px 30px #5C86CB2E",
    borderRadius: "4px",
    backgroundColor: "white",
    height: "100%",
    padding: "16px",
  },
  requestDetails: {
    boxShadow: "0px 3px 30px #5C86CB2E",
    borderRadius: "4px",
    backgroundColor: "white",
    height: "100%",
  },
  cardImg: {
    width: "32px",
    height: "32px",
  },
  cardNumber: {
    color: "#091B29",
    fontSize: "24px",
    fontWeight: "bold",
  },
  cardLabel: {
    color: "#091B29",
    fontSize: "14px",
  },
  generalReq: {
    color: "#091B29",
    fontSize: "14px",
    fontWeight: "bold",
  },
  generalNumber: {
    color: "#091B29",
    fontSize: "20px",
    fontWeight: "bold",
  },
  borderRightStyling: {
    height: "21px",
    margin: "auto 0 auto 0 !important",
  },
  parentRequest: {
    borderRadius: "4px",
    backgroundColor: "#F5F7FA",
    padding: "16px",
    display: "inline-flex",
  },
  switchingTab: {
    padding: "0px 0px 0px 0px",
    marginRight: "16px",
  },
  tabParent: {
    paddingLeft: "16px",
  },
  requestNumbers: {
    fontSize: "14px",
    fontWeight: "bold",
  },
  viewAll: {
    fontSize: "14px",
    color: "#5078E1",
  },
  searchParent: {},
  waterLeak: {
    color: "#606B7B",
    fontSize: "14px",
    fontWeight: "bold",
  },
  dateAndType: {
    color: "#7D8692",
    fontSize: "12px",
    fontWeight: "bold",
  },
  dot: {
    height: "6px",
    width: "6px",
    backgroundColor: "#bbb",
    borderRadius: "50%",
    display: "inline-block",
    marginLeft: "8px",
    marginRight: "8px",
  },
  overflowTypes: {
    maxHeight: "169px",
    backgroundColor: "white",
    overflow: "auto",
    "&::-webkit-scrollbar": {
      display: "none",
    },
  },
  tableLabel: {
    fontSize: "16px",
    color: "#091B29",
    fontWeight: "bold",
  },
  tableWithAddIcon: {
    position: "relative",
  },
  addIconButton: {
    position: "absolute",
    bottom: "-45px",
    right: "-22px",
    cursor: "pointer",
  },
  headerCreate: {
    boxShadow: "0px 10px 25px #0000000A",
    background: "white",
    padding: "8px 22px",
    marginTop: "62px",
    marginBottom: "16px",
  },
  headerText: {
    color: "#091B29",
    fontSize: "16px",
    fontWeight: "bold",
  },
  nextBtn: {
    backgroundColor: "#5078e1",
    color: "#ffffff",
    "&:hover": {
      backgroundColor: "#5078e1",
      color: "#ffffff",
    },
  },
}));

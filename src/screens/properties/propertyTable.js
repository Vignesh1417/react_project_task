import React from "react";
import { Typography, Box, Stack, Grid, Button } from "@mui/material";
import { useStyles } from "./style";
import { Search } from "../../components/search";
import { FilterIcon } from "../../assets";
import TableComp from "../../components/tableComp/tableComp";
import { AppRoutes } from "../../router/routes";
import { useNavigate } from "react-router-dom";

const header = [
  "Property Num",
  "Property Name",
  "Company Name",
  "Location",
  "Revenue Type",
  "Property Type",
  "Status",
];

const tableRowData = [
  {
    propertyNum: "PROP_21",
    propertyName: "Rubix Apartment",
    companyName: "Property Automate 102",
    location: "T.Nagar, Chennai",
    revenueType: "Sale",
    propertyType: "Apartment",
    status: "Active",
  },
  {
    propertyNum: "PROP_22",
    propertyName: "Apartment 2",
    companyName: "Property Automate 102",
    location: "T.Nagar, Chennai",
    revenueType: "Lease",
    propertyType: "Apartment",
    status: "Deactive",
  },
  {
    propertyNum: "PROP_23",
    propertyName: "Apartment 3",
    companyName: "Property Automate 102",
    location: "T.Nagar, Chennai",
    revenueType: "Maintain",
    propertyType: "Apartment",
    status: "Active",
  },
  {
    propertyNum: "PROP_24",
    propertyName: "Apartment 4",
    companyName: "Property Automate 102",
    location: "T.Nagar, Chennai",
    revenueType: "Sale",
    propertyType: "Apartment",
    status: "Active",
  },
];

const tableData = [
  { type: "TEXT", name: "propertyNum" },
  { type: "TEXT", name: "propertyName" },
  { type: "TEXT", name: "companyName" },
  { type: "TEXT", name: "location" },
  { type: "TEXT", name: "revenueType" },
  { type: "PROPERTY_TYPE", name: "propertyType" },
  { type: "STATUS", name: "status" },
];

export const TableProperty = (props) => {
  const classes = useStyles();

  const navigate = useNavigate();

  const handleClickRow = () => {
    navigate(AppRoutes.updatedScreen);
  };

  return (
    <Box className={classes.root}>
      <Stack
        direction="row"
        spacing={1}
        alignItems="center"
        justifyContent="space-between"
        className={classes.headerCreate}
      >
        <Typography className={classes.headerText}>Properties</Typography>
        <Button className={classes.nextBtn} onClick={handleClickRow}>
          Next
        </Button>
      </Stack>
      <Box className={classes.createGraphCard}>
        <Grid container spacing={2}>
          <Grid item xs={12} sm={12} md={12} lg={12}>
            <Box className={classes.propertyDetails}>
              <Stack>
                <Stack
                  direction={"row"}
                  alignItems={"center"}
                  justifyContent={"space-between"}
                  pb={2}
                >
                  <Search />
                  <FilterIcon />
                </Stack>
                <Stack className={classes.tableWithAddIcon}>
                  <TableComp
                    noAddNew
                    header={header}
                    tableData={tableData}
                    data={tableRowData}
                  />
                </Stack>
              </Stack>
            </Box>
          </Grid>
        </Grid>
      </Box>
    </Box>
  );
};

import React from "react";
import { AppTheme } from "./App.theme";
import RouterApp from "./router";
import { CssBaseline } from "@mui/material";


const App = () => {
  return (

    <AppTheme>
      <CssBaseline />
      <RouterApp />
    </AppTheme>
  );
}
export default App;
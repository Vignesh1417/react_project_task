import React from "react";
import { makeStyles } from "@mui/styles";
import { TopNavBar } from "../components/navbars/topNavbar/topNavbar";
import SideNavBar from "../components/navbars/sideNavbar/sideNavbar";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    height: "100%",
  },
  content: {
    width: "100%",
    height: "calc(100% - 64px)",
    overflow: "auto",
    [theme.breakpoints.up("sm")]: {
      paddingLeft: 71,
    },
    [theme.breakpoints.down("md")]: {
      paddingLeft: 0,
    },
  },
  topNavbar: {},
  sideNavbar: {
    [theme.breakpoints.down("md")]: {
      display: "none",
    },
  },
}));

const withNavBars = (Component) => (props) => {
  const classes = useStyles({ props });

  return (
    <div className={classes.root}>
      <div className={classes.topNavbar}>
        <TopNavBar />
      </div>

      {
        <div className={classes.sideNavbar}>
          <SideNavBar />
        </div>
      }

      <div className={classes.content}>
        <Component {...props}>{props.children}</Component>
      </div>
    </div>
  );
};

export default withNavBars;

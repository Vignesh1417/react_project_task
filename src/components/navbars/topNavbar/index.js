import React from "react";
import { TopNavBar } from "./topNavbar";

class TopNavbarParent extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return <TopNavBar />;
  }
}

export default TopNavbarParent;

import {
  AppBar,
  Avatar,
  Box,
  Button,
  Divider,
  Drawer,
  IconButton,
  Popover,
  Stack,
  Toolbar,
  Typography,
} from "@mui/material";
import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { Notification } from "../../../assets";
import { AppRoutes } from "../../../router/routes";
import { LocalStorageKeys } from "../../../utils";
import { AdminCard } from "../../adminCard";
import SideNavBar from "../sideNavbar/sideNavbar";
import TopNavbarStyles from "./style";

export const TopNavBar = (props) => {
  const classes = TopNavbarStyles();

  const navigate = useNavigate();

  const [anchorEl, setAnchorEl] = React.useState(null);

  const onLogin = () => {
    localStorage.setItem(LocalStorageKeys.authToken, "authtoken");
    navigate(AppRoutes.login);
  };

  const handlePopoverOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handlePopoverClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);

  const [state, setState] = useState({
    openSideNavBar: false,
  });

  const toggleSideNavBar = () => {
    setState({
      ...state,
      openSideNavBar: !state.openSideNavBar,
    });
  };

  return (
    <>
      <div className={classes.grow}>
        <AppBar position="static" className={classes.appBar}>
          <Toolbar className={classes.toolbarHeight}>
            <Stack
              direction="row"
              divider={
                <Divider
                  orientation="vertical"
                  flexItem
                  sx={{
                    borderColor: "white",
                    height: "15px",
                    margin: "auto !important",
                  }}
                />
              }
              spacing={2}
              className={classes.titleContainer}
              alignItems="center"
            >
              <IconButton onClick={onLogin}>
                <img
                  src="images/DNT Logo White-04@2x.png"
                  alt="Logo Image"
                  className={classes.logoTop}
                />
              </IconButton>

              <Typography className={classes.management}>
                PROPERTY MANAGEMENT SOLUTION
              </Typography>
            </Stack>

            <div className={classes.grow}>
              <Stack
                direction="row"
                divider={
                  <Divider
                    orientation="vertical"
                    flexItem
                    sx={{
                      borderColor: "white",
                      height: "22px",
                      margin: "auto !important",
                    }}
                  />
                }
                spacing={1}
                alignItems="center"
                className={classes.navbarTopLeftDropdown}
              >
                <IconButton
                  aria-label="logout button"
                  aria-controls={"logout_button"}
                  aria-haspopup="true"
                  color="inherit"
                  size="small"
                >
                  <Notification />
                </IconButton>

                <Stack
                  direction="row"
                  alignItems="center"
                  spacing={1}
                  aria-owns={open ? "mouse-over-popover" : undefined}
                  aria-haspopup="true"
                  onMouseEnter={handlePopoverOpen}
                  onMouseLeave={handlePopoverClose}
                >
                  <Avatar
                    alt="Remy Sharp"
                    src=""
                    sx={{ width: 31, height: 31 }}
                  />
                  <Box alignItems="center">
                    <Typography variant="body1" className={classes.adminName}>
                      Bala Ganesh
                    </Typography>
                    <Typography
                      variant="body2"
                      className={classes.adminOccupation}
                    >
                      Super Admin
                    </Typography>
                  </Box>
                  <IconButton>
                    <img src="images/downExpand.png" alt="expandDown" />
                  </IconButton>
                </Stack>
              </Stack>

              <Stack>
                <Drawer
                  open={state.openSideNavBar}
                  variant={"temporary"}
                  anchor="left"
                  onClose={toggleSideNavBar}
                >
                  <div style={{ width: 240 }}>
                    <SideNavBar isMobile={true} />
                  </div>
                </Drawer>
                <Popover
                  className={classes.popOverCard}
                  id="mouse-over-popover"
                  sx={{
                    pointerEvents: "none",
                  }}
                  open={open}
                  anchorEl={anchorEl}
                  anchorOrigin={{
                    vertical: "bottom",
                    horizontal: "left",
                  }}
                  transformOrigin={{
                    vertical: "top",
                    horizontal: "left",
                  }}
                  onClose={handlePopoverClose}
                  disableRestoreFocus
                >
                  <div className={classes.mainCard}>
                    <div className={classes.firstRow}>
                      <div className={classes.avatarImage}>
                        <Avatar className={classes.avatarImg} />
                      </div>
                      <div className={classes.textGroup}>
                        <div className={classes.adminName}>
                          <Typography>Bala Ganesh</Typography>
                        </div>
                        <div className={classes.email}>
                          <Typography>Balaganesh@gmail.com</Typography>
                        </div>
                        <div className={classes.admin}>
                          <Typography>Super Admin</Typography>
                        </div>
                      </div>
                    </div>
                    <div className={classes.secondRow}>
                      <div className={classes.roles}>
                        <Typography>ROLES</Typography>
                      </div>
                      <div className={classes.adminCards}>
                        <AdminCard />
                      </div>
                    </div>
                    <div className={classes.thirdRow}>
                      <div className={classes.hoverText}>
                        <Typography>My Profile</Typography>
                      </div>
                      <div className={classes.hoverText}>
                        <Typography>Privacy Policy</Typography>
                      </div>
                      <div className={classes.hoverText}>
                        <Typography>Terms and Conditions</Typography>
                      </div>
                      <div className={classes.signBtn}>
                        <Button>Sign Out</Button>
                      </div>
                    </div>
                  </div>
                </Popover>
              </Stack>
            </div>
          </Toolbar>
        </AppBar>
      </div>
    </>
  );
};

import { makeStyles } from "@mui/styles";

const TopNavbarStyles = makeStyles((theme) => ({
  grow: {
    zIndex: theme.zIndex.drawer + 1,
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    backgroundColor: "#1C1C1C",
    position: "fixed",
  },
  toolbarHeight: {
    minHeight: "57px",
    paddingLeft: "8px",
    justifyContent: "space-between",
  },
  title: {
    display: "block",
  },
  menuIcon: {
    [theme.breakpoints.up("md")]: {
      display: "none",
    },
  },
  navbarTopLeftDropdown: {
    [theme.breakpoints.down("sm")]: {
      display: "none",
    },
  },

  logoTop: {
    width: "115px",
  },
  management: {
    fontSize: "10px",
  },
  adminOccupation: {
    fontSize: "10px",
  },
  mainCard: {
    padding: "20px",
  },
  firstRow: {
    display: "flex",
  },
  avatarImg: {
    width: "60px !important",
    height: "60px !important",
    marginRight: "20px",
  },
  adminName: {
    fontSize: "12px",
    "& p": {
      fontWeight: 600,
      fontSize: "15px",
    },
  },
  email: {
    "& p": {
      fontSize: "12px",
      color: "#CED3DD",
    },
  },
  admin: {
    textAlign: "center",
    marginTop: "5px",
    "& p": {
      fontSize: "12px",
      fontWeight: 600,
      color: "#FF4B4B",
      backgroundColor: "#ffcbcb",
      borderRadius: "3px",
    },
  },
  roles: {
    marginTop: "20px",
    marginBottom: "15px",
    "& p": {
      fontSize: "12px",
      fontWeight: 600,
      color: "#98A0AC",
    },
  },
  popOverCard: {
    "& .MuiPaper-elevation8": {
      width: "350px",
    },
  },
  signBtn: {
    marginTop: "10px",
    "& button": {
      color: "#FF4B4B",
      border: "1px solid #FF4B4B",
      padding: "5px 15px",
      borderRadius: "8px",
    },
  },
  hoverText: {
    color: "#98A0AC",
    fontSize: "13px",
    fontWeight: 600,
    marginBottom: "5px",
  },
}));

export default TopNavbarStyles;

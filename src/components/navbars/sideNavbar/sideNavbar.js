// import {
//   Divider,
//   List,
//   ListItem,
//   ListItemIcon,
//   ListItemText,
//   Paper,
//   Stack,
// } from "@mui/material";
// import React from "react";
// import { matchPath, useLocation, useNavigate } from "react-router-dom";
// import { DownArrow, FourBoxIcon, Leads } from "../../../assets";
// import sideNavbarStyles from "./style";

// export const SideNavBar = (props) => {
//   const classes = sideNavbarStyles(props);

//   const navigate = useNavigate();
//   const location = useLocation();

//   const onListClick = (data) => {
//     if (data.link) {
//       navigate(data.link);
//     }
//   };

//   const isSelected = (data) => {
//     if (data.link) {
//       return matchPath(location.pathname, {
//         path: data.link,
//       });
//     }
//   };
//   return (
//     <>
//       <div className={classes.root}>
//         <Paper className={classes.drawer} square>
//           <div className={classes.drawerContainer}>
//             <List>
//               {[].map((navBar, index) => {
//                 return (
//                   <>
//                     <ListItem
//                       button
//                       onClick={(e) => onListClick(navBar)}
//                       key={index}
//                       selected={isSelected(navBar)}
//                     >
//                       {/* <ListItemIcon>{navBar.icon}</ListItemIcon> */}

//                       <ListItemText primary={navBar.name} />
//                     </ListItem>
//                   </>
//                 );
//               })}
//             </List>
//           </div>

//           <Stack
//             direction="column"
//             divider={
//               <Divider
//                 orientation="horizontal"
//                 flexItem
//                 sx={{
//                   borderColor: "#FFFFFF",
//                   width: "21px",
//                   margin: "auto !important",
//                   marginTop: "23px !important",
//                 }}
//                 spacing={2}
//                 alignItems="center"
//               />
//             }
//           >
//             <div style={{ textAlign: "-webkit-center" }}>
//               <div className={classes.rightArrow}>
//                 <DownArrow />
//               </div>
//             </div>
//             <Stack direction="column" spacing={1} alignItems="center">
//               <FourBoxIcon />
//               <Leads />
//             </Stack>
//           </Stack>
//         </Paper>
//       </div>
//     </>
//   );
// };

//import useState hook to create menu collapse state
import React, { useState } from "react";

//import react pro sidebar components
import {
  ProSidebar,
  Menu,
  MenuItem,
  SidebarHeader,
  SidebarContent,
} from "react-pro-sidebar";

//import sidebar css from react-pro-sidebar module and our custom css
import "react-pro-sidebar/dist/css/styles.css";
import "./sideBar.css";
import GroupHome from "../../../assets/groupHomeIcon/groupHomeIcon";
import { Leads } from "../../../assets";
import ExpandIcon from "../../../assets/expandIcon/expandIcon";
import { Divider } from "@mui/material";

const SideNavBar = () => {
  //create initial menuCollapse state using useState hook
  const [menuCollapse, setMenuCollapse] = useState(false);

  //create a custom function that will change menucollapse state from false to true and true to false
  const menuIconClick = () => {
    //condition checking to change state from true to false and vice versa
    menuCollapse ? setMenuCollapse(false) : setMenuCollapse(true);
  };

  return (
    <>
      <div id="header">
        {/* collapsed props to change menu size using menucollapse state */}
        <ProSidebar collapsed={!menuCollapse}>
          <SidebarHeader>
            <div className="logotext">
              {/* small and big change using menucollapse state */}
              <p>{menuCollapse ? "Logo" : "Big Logo"}</p>
            </div>
            <div className="closemenu" onClick={menuIconClick}>
              {/* changing menu collapse icon on click */}
              {/* {menuCollapse ? <FiArrowRightCircle /> : <FiArrowLeftCircle />} */}
              <ExpandIcon />
            </div>
          </SidebarHeader>
          <Divider />
          <SidebarContent>
            <Menu iconShape="square">
              <MenuItem active={true} icon={<GroupHome />}>
                Dashboard
              </MenuItem>
              <MenuItem icon={<Leads />}>Update Screen</MenuItem>
            </Menu>
          </SidebarContent>
        </ProSidebar>
      </div>
    </>
  );
};

export default SideNavBar;

import { makeStyles } from "@mui/styles";
const drawerWidth = 56;

const sideNavbarStyles = makeStyles((theme) => ({
  root: {
    width: (props) => (props?.isMobile ? 240 : drawerWidth),
    position: "absolute",
  },

  drawer: {
    width: "72px",
    height: "100vh",
    backgroundColor: "#333333",
    position: "fixed",
    marginTop: "58px",
  },

  rightArrow: {
    textAlign: "center",
    width: "26px",
    height: "26px",
    backgroundColor: "#5078E1",
    lineHeight: "26px",
    borderRadius: "50%",
    "&:hover": {
      cursor: "pointer",
    },
  },
}));

export default sideNavbarStyles;

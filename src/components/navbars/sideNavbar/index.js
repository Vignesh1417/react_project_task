import React from "react";
import { SideNavBar } from "./sideNavbar";

class SideNavBarParent extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return <SideNavBar {...this.props} />;
  }
}

export default SideNavBarParent;

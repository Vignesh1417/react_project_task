import { makeStyles } from "@mui/styles";

export const TextBoxStyles = makeStyles((theme) => ({
  label: {
    color: "#98A0AC",
    fontSize: "12px",
  },
  textBox: {
    "& .css-19qh8xo-MuiInputBase-input-MuiOutlinedInput-input": {
      fontSize: "14px",
    },
    "& .css-1ms0xco-MuiInputBase-root-MuiOutlinedInput-root": {
      borderRadius: "10px",
      fontSize: "14px",
    },
    "& .css-1t8l2tu-MuiInputBase-input-MuiOutlinedInput-input": {
      padding: "13.5px 14px",
      fontSize: "14px",
      fontWeight: "bold",
      color: "#5A5A5A",
    },
    "& .css-9rcej8-MuiInputBase-root-MuiOutlinedInput-root": {
      borderRadius: "10px",
    },
  },
  inputAdornment: {
    "& .css-15e6olr-MuiTypography-root": {
      fontSize: "12px",
      color: "#98A0AC",
    },
  },
}));

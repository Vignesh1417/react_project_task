import React from "react";
import { TextField, Stack, InputAdornment } from "@mui/material";
import Box from "@mui/material/Box";
import { TextBoxStyles } from "./style";

export const TextBox = ({
  label = "",
  placeholder = "",
  disable = "",
  adornment = "",
  defaultValue = "",
}) => {
  const classes = TextBoxStyles();

  return (
    <Box className={classes.root}>
      <Stack direction="column" spacing={1}>
        <label className={classes.label}>{label ?? ""}</label>
        <TextField
          disabled={disable ?? ""}
          size={"small"}
          id="outlined-basic"
          variant="outlined"
          placeholder={placeholder ?? ""}
          className={classes.textBox}
          defaultValue={defaultValue}
          InputProps={{
            endAdornment: (
              <InputAdornment className={classes.inputAdornment} position="end">
                {adornment ?? ""}
              </InputAdornment>
            ),
          }}
        />
      </Stack>
    </Box>
  );
};

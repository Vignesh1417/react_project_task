import React from "react";
import Box from "@mui/material/Box";
import { useStyles } from "./style";
import Alert from "@mui/material/Alert";
import CloseIcon from "@mui/icons-material/Close";
import { IconButton } from "@mui/material";
import Collapse from "@mui/material/Collapse";

export const AlertSuccess = ({ val = {} }) => {
  const [open, setOpen] = React.useState(true);

  const classes = useStyles();

  return (
    <Box className={classes.root}>
      <Box sx={{ width: "100%" }}>
        <Collapse in={open}>
          <Alert
            variant="filled"
            severity="success"
            action={
              <IconButton
                aria-label="close"
                color="inherit"
                size="small"
                onClick={() => {
                  setOpen(false);
                }}
              >
                <CloseIcon fontSize="inherit" />
              </IconButton>
            }
            sx={{ mb: 2 }}
          >
            Property Created Successfully
          </Alert>
        </Collapse>
      </Box>
    </Box>
  );
};

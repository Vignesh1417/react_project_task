import React from "react";
import { Box, TextField, Stack } from "@mui/material";
import { useStyles } from "./style";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";

export const DateShower = ({ label = "", placeholder = {} }) => {
  const classes = useStyles();
  const [value, setValue] = React.useState();

  return (
    <Box className={classes.root}>
      <Stack>
        <label className={classes.label}>{label ?? ""}</label>
        <LocalizationProvider dateAdapter={AdapterDayjs}>
          <DatePicker
            className={classes.dateStyling}
            value={value}
            onChange={(newValue) => {
              setValue(newValue);
            }}
            renderInput={(params) => <TextField {...params} />}
          />
        </LocalizationProvider>
      </Stack>
    </Box>
  );
};

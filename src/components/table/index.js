import React from "react";
import {
  Box,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
} from "@mui/material";
import { useStyles } from "./style";

function createData(
  propertyId,
  propertyName,
  totalUnits,
  occupiedUnits,
  occupancyPercentage
) {
  return {
    propertyId,
    propertyName,
    totalUnits,
    occupiedUnits,
    occupancyPercentage,
  };
}

const rows = [
  createData("prop 001", "Tysons", 24, 40, "90%"),
  createData("prop 002", "Rubix", 37, 43, "71%"),
  createData("prop 003", "Phoenix", 24, 60, "88%"),
  createData("prop 004", "Thapar", 67, 43, "88%"),
  createData("prop 005", "Marian", 49, 39, "96%"),
  createData("prop 003", "Phoenix", 24, 60, "88%"),
  createData("prop 004", "Thapar", 67, 43, "88%"),
  createData("prop 005", "Marian", 49, 39, "96%"),
];

export const CustomizeTable = ({ val = {} }) => {
  const classes = useStyles();

  return (
    <Box className={classes.root} sx={{ height: "100%" }}>
      <TableContainer component={Paper} className={classes.tableParent}>
        <Table aria-label="simple table" sx={{ borderCollapse: "initial" }}>
          <TableHead>
            <TableRow className={classes.tableHeader}>
              <TableCell align="left">Property Id</TableCell>
              <TableCell align="left">Property Name</TableCell>
              <TableCell align="left">Total Units</TableCell>
              <TableCell align="left">Occupied Units</TableCell>
              <TableCell align="left">Occupancy %</TableCell>
            </TableRow>
          </TableHead>
          <TableBody className={classes.tableBody}>
            {rows.map((row) => (
              <TableRow
                key={row.name}
                sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
              >
                <TableCell align="left">{row.propertyId}</TableCell>
                <TableCell align="left">{row.propertyName}</TableCell>
                <TableCell align="left">{row.totalUnits}</TableCell>
                <TableCell align="left">{row.occupiedUnits}</TableCell>
                <TableCell align="left">{row.occupancyPercentage}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </Box>
  );
};

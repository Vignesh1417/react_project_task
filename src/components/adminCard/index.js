import { Typography } from "@mui/material";
import React from "react";
import { AdminCardStyles } from "./style";

const cardData = [
  {
    letterName: "A",
    text: "Super Admin",
  },
  {
    letterName: "CM",
    text: "Community Manager",
  },
  {
    letterName: "SM",
    text: "Security Manager",
  },
  {
    letterName: "PM",
    text: "Property Manager",
  },
  {
    letterName: "ZM",
    text: "Zonal Manager",
  },
  {
    letterName: "CC",
    text: "Customer Care",
  },
];

export const AdminCard = (props) => {
  const classes = AdminCardStyles();
  return (
    <>
      <div className={classes.smallCardGroup}>
        {cardData?.map((val) => {
          return (
            <div className={classes.border}>
              <div
                className={
                  (val?.letterName === "A" && classes.redClr) ||
                  (val?.letterName === "CM" && classes.lightBlueClr) ||
                  (val?.letterName === "SM" && classes.yellowClr) ||
                  (val?.letterName === "PM" && classes.darkBlueClr) ||
                  (val?.letterName === "ZM" && classes.greenClr) ||
                  (val?.letterName === "CC" && classes.yellowClr)
                }
              >
                <Typography>{val?.letterName}</Typography>
              </div>
              <div className={classes.text}>
                <Typography>{val?.text}</Typography>
              </div>
            </div>
          );
        })}
      </div>
    </>
  );
};

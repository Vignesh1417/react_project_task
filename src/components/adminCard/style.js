import { makeStyles } from "@mui/styles";

export const AdminCardStyles = makeStyles((theme) => ({
  redClr: {
    textAlign: "-webkit-center",
    "& p": {
      fontSize: "12px",
      width: "30px",
      height: "30px",
      textAlign: "center",
      lineHeight: "30px",
      fontWeight: 600,
      color: "#FF4B4B",
      backgroundColor: "#ffcbcb",
      borderRadius: "50%",
    },
  },
  lightBlueClr: {
    textAlign: "-webkit-center",
    "& p": {
      fontSize: "12px",
      width: "30px",
      height: "30px",
      textAlign: "center",
      lineHeight: "30px",
      fontWeight: 600,
      color: "#78B1FE",
      backgroundColor: "#d3e6ff",
      borderRadius: "50%",
    },
  },
  yellowClr: {
    textAlign: "-webkit-center",
    "& p": {
      fontSize: "12px",
      width: "30px",
      height: "30px",
      textAlign: "center",
      lineHeight: "30px",
      fontWeight: 600,
      color: "#F3E137",
      backgroundColor: "#ffface",
      borderRadius: "50%",
    },
  },
  darkBlueClr: {
    textAlign: "-webkit-center",
    "& p": {
      fontSize: "12px",
      width: "30px",
      height: "30px",
      textAlign: "center",
      lineHeight: "30px",
      fontWeight: 600,
      color: "#5078E1",
      backgroundColor: "#c6d6ff",
      borderRadius: "50%",
    },
  },
  greenClr: {
    textAlign: "-webkit-center",
    "& p": {
      fontSize: "12px",
      width: "30px",
      height: "30px",
      textAlign: "center",
      lineHeight: "30px",
      fontWeight: 600,
      color: "#5AC782",
      backgroundColor: "#dfffeb",
      borderRadius: "50%",
    },
  },
  border: {
    width: "92px",
    border: "1px solid #98A0AC",
    borderRadius: "8px",
    marginBottom: "20px",
    padding: "10px",
  },
  smallCardGroup: {
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "space-between",
    textAlign: "center",
  },
  text: {
    marginTop: "7px",
    "& p": {
      fontSize: "13px",
      fontWeight: 600,
    },
  },
}));

import React from 'react';
import { TextField, Typography } from '@mui/material';
import { useStyles } from "./style";

export const FormInput = (props) => { 
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <TextField
                className={classes.textField}
                variant="outlined"
                value={props?.value}
                onChange={props?.onChange}
                fullWidth
                size="small"
            />
            {props.error&&
            <Typography className={classes.errorMsg}> {props?.errorMsg}</Typography>
}
        </div>
    );
}
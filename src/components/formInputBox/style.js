import { makeStyles } from "@mui/styles";

export const useStyles = makeStyles((theme) => ({

    textField: {

        "& .MuiTextField-root" :{
            borderRadius:"8px",
            "& input":{
                padding:"10px 14px"
            }
        }
    },
    errorMsg:{
        color:"red",
        fontSize:"12px",
    }
}))
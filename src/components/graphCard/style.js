import { makeStyles } from "@mui/styles";

export const GraphCardStyles = makeStyles((theme) => ({
  encloseIcon: {
    width: "18px",
    height: "18px",
  },
  cardParent: {
    boxShadow: "0px 3px 30px #5C86CB2E",
    borderRadius: "4px",
    backgroundColor: "white",
    padding: "10px",
    height: "100%",
  },
  graphLabel: {
    fontSize: "16px",
    color: "#091B29",
    fontWeight: "bold",
  },
}));

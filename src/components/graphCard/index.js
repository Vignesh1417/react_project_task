import React, { Component } from "react";
import Chart from "react-apexcharts";

class Charts extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <div id="chart">
        <Chart {...this.props.data} />
      </div>
    );
  }
}
export default Charts;

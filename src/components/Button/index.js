import React from 'react';
import {Box,Button }from '@mui/material';
import { useStyles } from "./style";

export const ButtonType = ({
    text = {},
    variant = {},
    color = {}

}) => {

    const classes = useStyles();


    return <Box className={classes.root}>
            <Button variant={variant?? ""} className={classes.Button} >{text}</Button>
    </Box>      

}
import { makeStyles } from "@mui/styles";

export const useStyles = makeStyles((theme) => ({
    Button:{
        border:`1px solid ${theme.palette.secondary.main}`,
        backgroundColor:`${theme.palette.secondary.main}`,
        color:"white"
    }

}))
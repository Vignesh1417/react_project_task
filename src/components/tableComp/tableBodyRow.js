import { Popover, TableCell, TableRow, Typography } from "@mui/material";
import React from "react";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/Delete";
import parse from "html-react-parser";
import { useStyles } from "./styles";
import MoreVertIcon from "@mui/icons-material/MoreVert";

const TableBodyRow = ({
  tableData = [],
  rowData = {},
  rowIndex = null,
  handleViewStores = () => null,
  handleDelete = () => null,
  handleEdit = () => null,
}) => {
  const classes = useStyles();

  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const id = open ? "simple-popover" : undefined;

  const getComponent = (data) => {
    switch (data.type) {
      case "TEXT": {
        return (
          <div
            className={classes.textStyle}
            style={{
              width: data?.customWidth ? data?.customWidth : "",
              lineBreak: data?.customWidth ? "anywhere" : "unset",
            }}
          >
            {data.rowData ? data.rowData : "-"}
          </div>
        );
      }
      case "HTMLTEXT": {
        return <div>{parse(data.rowData)}</div>;
      }
      case "INCREMENT": {
        return <div>{data.rowIndex + 1}</div>;
      }
      case "TOTAL_STORES": {
        return <div>{data?.rowData?.length}</div>;
      }
      case "VIEW_STORES": {
        return (
          <div
            style={{
              color: "blue",
              cursor: "pointer",
              textDecoration: "underline",
            }}
            onClick={data?.handleViewStores}
          >
            View Stores
          </div>
        );
      }
      case "MBL_IMAGE": {
        return (
          <img
            alt="nacimages"
            src={data.rowData}
            style={{ width: "150px", height: "auto" }}
          />
        );
      }
      case "WEB_IMAGE": {
        return (
          <img
            alt="nacimages"
            src={data.rowData}
            style={{ width: "150px", height: "auto" }}
          />
        );
      }
      case "ACTION": {
        return (
          <div
            style={{
              display: "flex",
              alignItems: "center",
              gap: 6,
            }}
          >
            <EditIcon
              onClick={data?.handleEdit}
              style={{ cursor: "pointer" }}
            />
            <DeleteIcon
              onClick={data?.handleDelete}
              style={{ color: "red", cursor: "pointer" }}
            />
          </div>
        );
      }
      case "EDIT": {
        return (
          <EditIcon style={{ cursor: "pointer" }} onClick={data?.handleEdit} />
        );
      }
      case "BUTTON_ARRAY": {
        return (
          <div>
            {data.rowData.map((val) => (
              <div style={{ paddingBottom: "4px" }}>
                <div>{val.name}</div>
                {val.url.length > 0 && (
                  <div style={{ color: "blue" }}>{val.url}</div>
                )}
              </div>
            ))}
          </div>
        );
      }
      case "ARRAYTEXT": {
        return data?.rowData?.map((_) => {
          return (
            <Typography>
              <div>{_.name}</div>
              {_.url.length > 0 && <div style={{ color: "blue" }}>{_.url}</div>}
            </Typography>
          );
        });
      }
      case "PROPERTY_TYPE": {
        return (
          <div className={classes.prop_type}>
            <Typography>{data.rowData ? data.rowData : "-"}</Typography>
          </div>
        );
      }
      case "STATUS": {
        return (
          <div className={classes.statusTotal}>
            <div
              className={
                data?.rowData === "Active" ? classes.status : classes.statusClr
              }
            >
              <Typography>{data.rowData ? data.rowData : "-"}</Typography>
            </div>
            <div className={classes.dotIcon}>
              <MoreVertIcon aria-describedby={id} onClick={handleClick} />

              <Popover
                id={id}
                open={open}
                anchorEl={anchorEl}
                onClose={handleClose}
                anchorOrigin={{
                  vertical: "bottom",
                  horizontal: "left",
                }}
              >
                <div className={classes.popOver} sx={{ p: 2 }}>
                  <Typography>Edit</Typography>
                  <Typography>Inactive</Typography>
                  <Typography>Delete</Typography>
                </div>
              </Popover>
            </div>
          </div>
        );
      }
      default: {
        return data.type;
      }
    }
  };
  return (
    <TableRow>
      {tableData.map((val, i) => (
        <TableCell>
          {getComponent({
            type: val.type,
            rowData: rowData[val.name],
            rowIndex: rowIndex,
            handleViewStores: (e) => {
              handleViewStores(e, rowData, rowIndex);
            },
            handleDelete: (e) => {
              handleDelete(e, rowData, rowIndex);
            },
            handleEdit: (e) => {
              handleEdit(e, rowData, rowIndex);
            },
            customWidth: val?.width,
          })}
        </TableCell>
      ))}
    </TableRow>
  );
};

export default TableBodyRow;

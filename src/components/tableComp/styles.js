import { makeStyles } from "@mui/styles";

export const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    marginTop: theme.spacing(3),
  },
  dialogPaper: {
    minWidth: "1200px",
  },
  dialogPaperMid: {
    minWidth: "800px",
  },
  dialogHeader: {
    backgroundColor: "#fff",
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
  dialogTitle: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
  imagecontainer: {
    display: "flex",
    "& > *": {
      margin: theme.spacing(1),
    },
  },
  table: {
    minWidth: 500,
    border: "none",
    borderRadius: "5px",
  },
  tableWrapper: {
    overflowX: "auto",
    backgroundColor: "#fff",
    marginBottom: "60px",
  },
  link: {
    display: "flex",
  },
  icon: {
    marginRight: theme.spacing(0.5),
    width: 20,
    height: 20,
  },
  link_style: {
    color: "#000",
  },

  prop_type: {
    textAlign: "center",
    "& p": {
      backgroundColor: "#78B1FE",
      color: "#FFFFFF",
      borderRadius: "4px",
    },
  },

  status: {
    "& p": {
      color: "#5AC782",
    },
  },

  statusClr: {
    "& p": {
      color: "#FF4B4B",
    },
  },

  textStyle: {
    fontWeight: 600,
    color: "#091B29",
  },

  theadRow: {
    backgroundColor: "#F2F4F7 !important",
    "& th": {
      backgroundColor: "#F2F4F7 !important",
      border: "0px",
      color: "#4E5A6B",
      fontWeight: 600,
    },
  },

  tableBodyCell: {
    "& td": {
      border: "0px",
      borderBottom: "2px solid #F2F4F7",
      padding: "10px 16px !important",
    },
  },

  statusTotal: {
    display: "flex",
    justifyContent: "space-between",
  },

  dotIcon: {
    "& svg": {
      cursor: "pointer",
    },
  },

  popOver: {
    padding: "10px",
    "& p": {
      borderBottom: "2px solid #F2F4F7",
      padding: "8px",
      cursor: "pointer",
    },
  },
}));

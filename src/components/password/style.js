import { makeStyles } from "@mui/styles";

export const PasswordStyles = makeStyles((theme) => ({
  label: {
    color: "#98A0AC",
    fontSize: "12px",
  },
  passwordBox: {
    "& .css-9rcej8-MuiInputBase-root-MuiOutlinedInput-root": {
      borderRadius: "10px",
    },
    "& .css-nxo287-MuiInputBase-input-MuiOutlinedInput-input": {
      padding: "8.5px 14px",
      fontSize: "14px",
    },
  },
  eyeIcon: {
    "& .css-i4bv87-MuiSvgIcon-root": {
      width: "20px",
      height: "20px",
    },
  },
}));

import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import {
  Home,
  Dashboard,
  LoginPage,
  NotFound,
  TableProperty,
  UpdatedScreen,
} from "../screens";
import PrivateRoute from "./privateRouter";
import { AppRoutes } from "./routes";

const RouterApp = (props) => {
  return (
    <BrowserRouter>
      <Routes>
        {/* Login Route */}

        <Route path={AppRoutes.login} element={<LoginPage />} />

        {/* Home Route */}

        <Route
          path={AppRoutes.home}
          element={
            <PrivateRoute path={AppRoutes.home}>
              <Home />
            </PrivateRoute>
          }
        />

        {/* Dashboard Route */}

        <Route path={AppRoutes.dashboardCard} element={<Dashboard />} />

        {/* Update Screen Route */}

        <Route path={AppRoutes.updatedScreen} element={<UpdatedScreen />} />

        {/* Prop Table Route */}

        <Route path={AppRoutes.propTable} element={<TableProperty />} />

        {/* Not Found Screen */}

        <Route path="*" element={<NotFound />} />
      </Routes>
    </BrowserRouter>
  );
};

export default RouterApp;

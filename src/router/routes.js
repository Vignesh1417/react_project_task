export const AppRoutes = {
  login: "/",
  home: "/homePage",
  dashboardCard: "/dashBoard",
  updatedScreen: "/updatedScreen",
  propTable: "/propTable",
};

import React from "react";
import {
  ThemeProvider,
  StyledEngineProvider,
  createTheme,
  responsiveFontSizes,
} from "@mui/material";
import { Themes } from "./utils";
import { ThemeContext } from "./contexts/themeContext";

export const AppTheme = (props) => {
  let [themeSettings, setThemeSettings] = React.useState({
    name: Themes.default,
  });

  const themeSetting = () => {
    let currentThemeJson;

    // json for the items which are selected
    switch (themeSettings.name) {
      case Themes.default:
        currentThemeJson = require("./themes/default.json");
        break;

      case Themes.dark:
        currentThemeJson = require("./themes/dark.json");
        break;

      default:
        currentThemeJson = require("./themes/default.json");
        break;
    }

    let currentTheme = createTheme(currentThemeJson);

    currentTheme = responsiveFontSizes(currentTheme);

    return currentTheme;
  };

  return (
    <ThemeContext.Provider value={{ ...themeSettings, setThemeSettings }}>
      <StyledEngineProvider injectFirst>
        {/* theme provider */}
        <ThemeProvider theme={themeSetting()}>{props.children}</ThemeProvider>
        {/* theme provider */}
      </StyledEngineProvider>
    </ThemeContext.Provider>
  );
};

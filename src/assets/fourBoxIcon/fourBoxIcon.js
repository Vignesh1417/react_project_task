import * as React from "react";

export const FourBoxIcon = (props) => (
  <svg xmlns="http://www.w3.org/2000/svg" width={30} height={30} {...props}>
    <g data-name="Group 8127">
      <path data-name="Rectangle 31" fill="none" d="M0 0h30v30H0z" />
      <g fill="#c1c5cb">
        <path
          data-name="Path 93404"
          d="M12.781 7H8.094A1.1 1.1 0 0 0 7 8.094v2.812A1.1 1.1 0 0 0 8.094 12h4.687a1.1 1.1 0 0 0 1.094-1.094V8.094A1.1 1.1 0 0 0 12.781 7Zm0 0"
        />
        <path
          data-name="Path 93405"
          d="M12.781 13.25H8.094A1.1 1.1 0 0 0 7 14.344v6.563a1.1 1.1 0 0 0 1.094 1.094h4.687a1.1 1.1 0 0 0 1.094-1.094v-6.562a1.1 1.1 0 0 0-1.094-1.095Zm0 0"
        />
        <path
          data-name="Path 93406"
          d="M20.906 17h-4.687a1.1 1.1 0 0 0-1.094 1.094v2.813a1.1 1.1 0 0 0 1.094 1.094h4.688a1.1 1.1 0 0 0 1.094-1.094v-2.812A1.1 1.1 0 0 0 20.906 17Zm0 0"
        />
        <path
          data-name="Path 93407"
          d="M20.906 7h-4.687a1.1 1.1 0 0 0-1.094 1.094v6.562a1.1 1.1 0 0 0 1.094 1.094h4.688a1.1 1.1 0 0 0 1.094-1.094V8.094A1.1 1.1 0 0 0 20.906 7Zm0 0"
        />
      </g>
    </g>
  </svg>
);

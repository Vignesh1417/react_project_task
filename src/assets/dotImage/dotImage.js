import * as React from "react";

const DotImage = (props) => (
  <svg xmlns="http://www.w3.org/2000/svg" width={146} height={140} {...props}>
    <g
      data-name="Group 5025"
      transform="translate(12326.527 12387.171)"
      opacity={0.56}
      fill="#2c3060"
    >
      <circle
        data-name="Ellipse 22"
        cx={4.5}
        cy={4.5}
        r={4.5}
        transform="translate(-12223.527 -12387.171)"
      />
      <ellipse
        data-name="Ellipse 38"
        cx={4}
        cy={4.5}
        rx={4}
        ry={4.5}
        transform="translate(-12188.527 -12387.171)"
      />
      <ellipse
        data-name="Ellipse 26"
        cx={4.5}
        cy={4}
        rx={4.5}
        ry={4}
        transform="translate(-12223.527 -12354.171)"
      />
      <circle
        data-name="Ellipse 39"
        cx={4}
        cy={4}
        r={4}
        transform="translate(-12188.527 -12354.171)"
      />
      <ellipse
        data-name="Ellipse 30"
        cx={4.5}
        cy={4}
        rx={4.5}
        ry={4}
        transform="translate(-12223.527 -12321.171)"
      />
      <circle
        data-name="Ellipse 40"
        cx={4}
        cy={4}
        r={4}
        transform="translate(-12188.527 -12321.171)"
      />
      <ellipse
        data-name="Ellipse 34"
        cx={4.5}
        cy={4}
        rx={4.5}
        ry={4}
        transform="translate(-12223.527 -12288.171)"
      />
      <circle
        data-name="Ellipse 43"
        cx={4.5}
        cy={4.5}
        r={4.5}
        transform="translate(-12223.527 -12256.171)"
      />
      <circle
        data-name="Ellipse 41"
        cx={4}
        cy={4}
        r={4}
        transform="translate(-12188.527 -12288.171)"
      />
      <ellipse
        data-name="Ellipse 42"
        cx={4}
        cy={4.5}
        rx={4}
        ry={4.5}
        transform="translate(-12188.527 -12256.171)"
      />
      <ellipse
        data-name="Ellipse 23"
        cx={4}
        cy={4.5}
        rx={4}
        ry={4.5}
        transform="translate(-12257.527 -12387.171)"
      />
      <circle
        data-name="Ellipse 27"
        cx={4}
        cy={4}
        r={4}
        transform="translate(-12257.527 -12354.171)"
      />
      <circle
        data-name="Ellipse 31"
        cx={4}
        cy={4}
        r={4}
        transform="translate(-12257.527 -12321.171)"
      />
      <circle
        data-name="Ellipse 35"
        cx={4}
        cy={4}
        r={4}
        transform="translate(-12257.527 -12288.171)"
      />
      <ellipse
        data-name="Ellipse 44"
        cx={4}
        cy={4.5}
        rx={4}
        ry={4.5}
        transform="translate(-12257.527 -12256.171)"
      />
      <circle
        data-name="Ellipse 24"
        cx={4.5}
        cy={4.5}
        r={4.5}
        transform="translate(-12292.527 -12387.171)"
      />
      <ellipse
        data-name="Ellipse 28"
        cx={4.5}
        cy={4}
        rx={4.5}
        ry={4}
        transform="translate(-12292.527 -12354.171)"
      />
      <ellipse
        data-name="Ellipse 32"
        cx={4.5}
        cy={4}
        rx={4.5}
        ry={4}
        transform="translate(-12292.527 -12321.171)"
      />
      <ellipse
        data-name="Ellipse 36"
        cx={4.5}
        cy={4}
        rx={4.5}
        ry={4}
        transform="translate(-12292.527 -12288.171)"
      />
      <circle
        data-name="Ellipse 45"
        cx={4.5}
        cy={4.5}
        r={4.5}
        transform="translate(-12292.527 -12256.171)"
      />
      <ellipse
        data-name="Ellipse 25"
        cx={4}
        cy={4.5}
        rx={4}
        ry={4.5}
        transform="translate(-12326.527 -12387.171)"
      />
      <circle
        data-name="Ellipse 29"
        cx={4}
        cy={4}
        r={4}
        transform="translate(-12326.527 -12354.171)"
      />
      <circle
        data-name="Ellipse 33"
        cx={4}
        cy={4}
        r={4}
        transform="translate(-12326.527 -12321.171)"
      />
      <circle
        data-name="Ellipse 37"
        cx={4}
        cy={4}
        r={4}
        transform="translate(-12326.527 -12288.171)"
      />
      <ellipse
        data-name="Ellipse 46"
        cx={4}
        cy={4.5}
        rx={4}
        ry={4.5}
        transform="translate(-12326.527 -12256.171)"
      />
    </g>
  </svg>
);

export default DotImage;

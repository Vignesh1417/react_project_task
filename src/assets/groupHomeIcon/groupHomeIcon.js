import * as React from "react";

const GroupHome = (props) => (
  <svg xmlns="http://www.w3.org/2000/svg" width={30} height={30} {...props}>
    <g data-name="Group 8127" transform="translate(-1523 -1001)">
      <rect
        data-name="Rectangle 31"
        width={30}
        height={30}
        rx={15}
        transform="translate(1523 1001)"
        fill="#5078e1"
      />
      <g fill="#fff">
        <path
          data-name="Path 93404"
          d="M1535.781 1008h-4.687a1.1 1.1 0 0 0-1.094 1.094v2.812a1.1 1.1 0 0 0 1.094 1.094h4.687a1.1 1.1 0 0 0 1.094-1.094v-2.812a1.1 1.1 0 0 0-1.094-1.094Zm0 0"
        />
        <path
          data-name="Path 93405"
          d="M1535.781 1014.25h-4.687a1.1 1.1 0 0 0-1.094 1.094v6.563a1.1 1.1 0 0 0 1.094 1.094h4.687a1.1 1.1 0 0 0 1.094-1.094v-6.562a1.1 1.1 0 0 0-1.094-1.095Zm0 0"
        />
        <path
          data-name="Path 93406"
          d="M1543.906 1018h-4.687a1.1 1.1 0 0 0-1.094 1.094v2.813a1.1 1.1 0 0 0 1.094 1.094h4.688a1.1 1.1 0 0 0 1.094-1.094v-2.812a1.1 1.1 0 0 0-1.095-1.095Zm0 0"
        />
        <path
          data-name="Path 93407"
          d="M1543.906 1008h-4.687a1.1 1.1 0 0 0-1.094 1.094v6.562a1.1 1.1 0 0 0 1.094 1.094h4.688a1.1 1.1 0 0 0 1.094-1.094v-6.562a1.1 1.1 0 0 0-1.095-1.094Zm0 0"
        />
      </g>
    </g>
  </svg>
);

export default GroupHome;
